$('.cabinet__user-edit').click(function(e) {
  e.preventDefault();
  $('.tab-pane').removeClass('show active');
  $('#profile').addClass('show active');
  $('.nav-link').removeClass('show active');
  $('#profile-tab').addClass('show active');
  $('.cabinet__table-wrapper').hide();
  $('.cabinet__table-form').fadeIn('fast');
});

$('.cabinet__table-form-back').click(function(e) {
  e.preventDefault();
  $('.cabinet__table-form').hide();
  $('.cabinet__table-wrapper').fadeIn('fast');
})
