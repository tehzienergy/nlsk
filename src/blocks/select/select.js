if ($('.select__input').length) {
  
  $('.select__input').each(function() {
    
    var container = $(this).closest('.select');

    $(this).select2({
      minimumResultsForSearch: -1,
      width: '100%',
      dropdownParent: container
    })
  })
}
